package com.backend.api.Service;

import com.backend.api.Model.User;
import com.backend.api.Model.rqt.UserInfoRqt;
import com.backend.api.Model.rqt.UserLoginRqt;
import com.backend.api.Service.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public ResponseEntity registerUser(UserLoginRqt userLoginRqt) {

        if (userLoginRqt.getUsername() == null || userLoginRqt.getPassword() == null || userLoginRqt.getPassword().trim().equals("") || userLoginRqt.getUsername().trim().equals("")) {
            return new ResponseEntity("Username/Password cannot be null/empty", HttpStatus.FORBIDDEN);
        }

        User user = userRepository.findUserByUsername(userLoginRqt.getUsername());
        if (user != null) {
            return new ResponseEntity("USER ALREADY EXIST", HttpStatus.CONFLICT);
        } else {
            User newUser = new User(userLoginRqt.getUsername(), userLoginRqt.getPassword());
            userRepository.save(newUser);
            return new ResponseEntity("User Successfully Registered", HttpStatus.OK);
        }

    }

    public ResponseEntity getUserById(Long id) {
        User user = userRepository.findUserById(id);
        if (user == null) {
            return new ResponseEntity("User Not Found", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(user, HttpStatus.OK);
        }
    }

    public ResponseEntity updateUserInformation(Long uuid, UserInfoRqt userReq) {
        User user = userRepository.findUserById(uuid);
        if (user == null) {
            return new ResponseEntity("User not found", HttpStatus.NOT_FOUND);
        } else {
            user.setEmail(userReq.getEmail());
            user.setPhone(userReq.getPhone());
            user.setName(userReq.getName());
            userRepository.save(user);
            return new ResponseEntity(user, HttpStatus.OK);
        }
    }

    public ResponseEntity userLogin(UserLoginRqt userLoginRqt) {
        if (userLoginRqt.getUsername() == null || userLoginRqt.getPassword() == null || userLoginRqt.getPassword().trim().equals("") || userLoginRqt.getUsername().trim().equals("")) {
            return new ResponseEntity("Username/Password cannot be null/empty", HttpStatus.FORBIDDEN);
        }
        User user = userRepository.findUserByUsername(userLoginRqt.getUsername());
        if (user != null) {
            boolean correctPassword = new BCryptPasswordEncoder().matches(userLoginRqt.getPassword(), user.getPassword());
            if (correctPassword) {
                user.setLoggedIn(true);
                userRepository.save(user);
                return new ResponseEntity(user, HttpStatus.OK);
            }

        }
        return new ResponseEntity("Incorrect Username and Password", HttpStatus.FORBIDDEN);
    }
}
