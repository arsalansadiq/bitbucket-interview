package com.backend.api.Controller;

import com.backend.api.Model.rqt.UserInfoRqt;
import com.backend.api.Model.rqt.UserLoginRqt;
import com.backend.api.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    //login
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Object> login(@RequestBody UserLoginRqt userLoginRqt) {
        ResponseEntity response = userService.userLogin(userLoginRqt);
        return response;
    }

    //register (POST)
    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Object> register(@RequestBody UserLoginRqt userLoginRqt) {
        ResponseEntity response = userService.registerUser(userLoginRqt);
        return response;
    }

    // get user by id(GET)
    @RequestMapping(value = "/users/{uuid}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Object> getUserById(@PathVariable Long uuid) {
        ResponseEntity response = userService.getUserById(uuid);
        return response;
    }


    // put user information by id (POST)
    @RequestMapping(value = "/users/{uuid}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Object> UpdateUserInformation(@PathVariable Long uuid, @RequestBody UserInfoRqt user) {
        ResponseEntity response = userService.updateUserInformation(uuid, user);
        return response;
    }


}
